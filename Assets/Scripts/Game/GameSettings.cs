namespace Game
{
    public static class GameSettings
    {
        public const float TackingItemTime = 0.5f;
        public const float SpawnAgentDelay = 1f;

        public static float PlacingItemTime = 2;
        public static float MoveSpeed = 1.5f;

    }
}