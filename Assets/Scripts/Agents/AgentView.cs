using System;
using Misc;
using UnityEngine;

namespace Agents
{
    public class AgentView : MonoBehaviour
    {
        [SerializeField]
        private Agent agent;

        [SerializeField]
        private Animator animator;

        private static readonly int Walking = Animator.StringToHash("Walking");
        private static readonly int TakingItem = Animator.StringToHash("TakingItem");
        private static readonly int Awaiting = Animator.StringToHash("Awaiting");
        private static readonly int PlacingItem = Animator.StringToHash("PlacingItem");

        public void Update()
        {
            animator.SetBool(Awaiting, agent.Status == AgentStatus.Awaiting);
            animator.SetBool(Walking, agent.Status == AgentStatus.Moving);
            animator.SetBool(TakingItem, agent.Status == AgentStatus.PickingItem);
            animator.SetBool(PlacingItem, agent.Status == AgentStatus.PlacingItem);

            var transformForward = agent.Direction.ToWorldPosition();
            
            if (transformForward != Vector3.zero)
                transform.forward = transformForward;
        }
    }
}