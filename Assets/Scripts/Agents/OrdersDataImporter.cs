using System;
using System.Collections.Generic;
using System.Linq;
using Map.Path;
using Misc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using UnityEngine;

namespace Agents
{
    [Serializable]
    public class OrdersDataImporter
    {
        [Serializable]
        public readonly struct OrderItemsData
        {
            public readonly int ID;
            public readonly int Amount;
            public readonly Vector2Int Position;

            public OrderItemsData(int id, int amount, Vector2Int position)
            {
                ID = id;
                Amount = amount;
                Position = position;
            }
            
            public static OrderItemsData FromJson(JToken token)
            {
                return new OrderItemsData(token["id"].Value<int>(),
                    token["amount"].Value<int>(), 
                    new Vector2Int(token["position"]["x"].Value<int>(),
                        token["position"]["y"].Value<int>())
                );
            }
        }

        [Serializable]
        public readonly struct OrderPathData
        {
            public readonly List<Vector2Int> Positions;

            public bool Exists => Positions != null;

            public OrderPathData(List<Vector2Int> positions)
            {
                Positions = positions;
            }

            public static OrderPathData FromJson(JToken token)
            {
                return new OrderPathData(token.Select(
                    t => new Vector2Int(t["x"].Value<int>(),
                        t["y"].Value<int>())).ToList());
            }

            public Path ToPath()
            {
                return new Path {positions = Positions};
            }
        }

        [Serializable]
        public readonly struct OrderData
        {
            public readonly List<OrderItemsData> Items;
            public readonly OrderPathData Path;

            public OrderData(List<OrderItemsData> items, OrderPathData path)
            {
                Items = items;
                
                Path = path;
            }

            public static OrderData FromJson(JToken token)
            {
                
                return new OrderData(token["items"].Select(OrderItemsData.FromJson).ToList(),
                    token["path"] != null ? 
                        OrderPathData.FromJson(token["path"]) :
                        new OrderPathData(null));
            }
        }
        

        public List<OrderData> Orders;
        public int AgentsCount;
        
        public OrdersDataImporter(string jsonText)
        {
            var json = JObject.Parse(jsonText);

            Orders = json["orders"].Select(OrderData.FromJson).ToList();
            
            AgentsCount = json["agentsCount"].Value<int>();
        }
    }
}