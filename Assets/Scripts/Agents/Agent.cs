using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Game;
using Gameplay;
using Map;
using Map.Path;
using Misc;
using Storage;
using UnityEngine;

namespace Agents
{
    public class Agent : MonoBehaviour
    {
        public Vector2Int Position => transform.position.ToPositionOnMap();
        public Vector2Int Direction { get; private set; } = Vector2Int.up;
        public int Distance { get; private set; }

        public AgentStatus Status { get; private set; } = AgentStatus.Awaiting;
        public Path MyPath => _path;        
        
        public Vector2Int NextWaypoint { get; private set; }
        public OrdersDataImporter.OrderItemsData NextOrderItems { get; private set; }

        public ItemContainer container;
        public float speed;
        
        private Path _path;
        private Order _order;

        public void StartAgent(Path path, List<OrdersDataImporter.OrderItemsData> order)
        {
            _path = path;
            NextWaypoint = _path.Next();

            _order = new Order(order);
            NextOrderItems = _order.Orders[0];
            
            StartCoroutine(UpdateAgent());
        }

        private void Update()
        {
            if (Status != AgentStatus.Awaiting)
                return;

            var nextOrder = AgentsGenerator.NextOrder;
            if (nextOrder != null)
            {

                StartAgent(
                    nextOrder.Value.Path.Exists ?
                        nextOrder.Value.Path.ToPath() :
                        AgentsGenerator.BasePath.CalculateAgentRoute(
                        new HashSet<Vector2Int>(nextOrder.Value.Items.Select(o => o.Position))
                    ), 
                    nextOrder.Value.Items);
            }
        }


        private IEnumerator UpdateAgent()
        {
            while (!_path.IsComplete)
            {
                if (!_order.IsComplete && NextOrderItems.Position.Adjacent(Position))
                {
                    var shelf = MapGenerator.Shelves[NextOrderItems.Position];

                    for (int i = 0; i < NextOrderItems.Amount; i++)
                    {
                        yield return TakeItem(shelf, NextOrderItems.ID);
                    }

                    NextOrderItems = _order.Next();
                }
                
                if (_order.IsComplete && MapGenerator.MapData.Map.Get(Position) == EntitiesOnMap.DropZone)
                {
                    yield return DropItems();
                }
                
                yield return Move(Position, NextWaypoint);

                NextWaypoint = _path.Next();
            }
            
            if (_order.IsComplete && MapGenerator.MapData.Map.Get(Position) == EntitiesOnMap.DropZone)
            {
                yield return DropItems();
            }
            
            yield return Move(Position, NextWaypoint);
            
            Status = AgentStatus.Awaiting;
        }

        private IEnumerator TakeItem(ItemContainer from, int itemID)
        {
            Status = AgentStatus.PickingItem;
            
            from.TryTakeItem(itemID, out var item);
            container.TryPlaceItem(item);
            yield return new WaitForSeconds(GameSettings.TackingItemTime);
        }

        private IEnumerator DropItems()
        {
            Status = AgentStatus.PlacingItem;

            foreach (var item in container.Items)
            {
                yield return item.Destroy();
            }
        }
        

        private IEnumerator Move(Vector2Int from, Vector2Int to)
        {
            Status = AgentStatus.Moving;
            
            Direction = NextWaypoint - Position;
            
            if (Direction.x != 0 && Direction.y != 0)
                throw new NotImplementedException("A*");


            Distance = (int) Direction.magnitude;
            Direction = Direction.FlattenAndNormalize();
            
            var fromWP = from.ToWorldPosition(.5f);
            var toWP = to.ToWorldPosition(.5f);

            float passedDistance = 0;
            while (passedDistance < Distance)
            {
                transform.position = Vector3.Lerp(fromWP, toWP, passedDistance / Distance);
                passedDistance += speed * Time.deltaTime;
                yield return null;
            }

            transform.position = toWP;
        }
        

    }
}