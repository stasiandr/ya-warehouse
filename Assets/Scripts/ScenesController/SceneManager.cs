using System;
using System.Collections;
using Agents;
using FileUpload;
using Map;
using Miscellaneous;
using StaticData;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.ResourceManagement.ResourceProviders;
using UnityEngine.SceneManagement;

namespace ScenesController
{
    public class SceneManager : MonoBehaviour
    {
        public AddressableLoader loader;
        
        public AssetReference playerScene;

        public AssetReference environmentScene;
        public AssetReference uiScene;

        public AssetReference moveableScene;
        
        private IEnumerator Start()
        {
            DontDestroyOnLoad(gameObject);
            while (!loader.Initialized)
                yield return null;

            yield return playerScene.LoadSceneAsync(LoadSceneMode.Additive);
            
            yield return environmentScene.LoadSceneAsync(LoadSceneMode.Additive);
            yield return uiScene.LoadSceneAsync(LoadSceneMode.Additive);
            
            var mapRequest = new FileUploadHelper.FileRequest();
            yield return mapRequest.FileDialogText();
            var mapRequestText = mapRequest.Result;
            
            var ordersRequest = new FileUploadHelper.FileRequest();
            yield return ordersRequest.FileDialogText();
            var ordersRequestText = ordersRequest.Result;
            
            yield return moveableScene.LoadSceneAsync(LoadSceneMode.Additive);

            var mapGenerator = FindObjectOfType<MapGenerator>();
            yield return mapGenerator.GenerateMap(new MapDataImporter(mapRequestText), new ItemsImporterData(mapRequestText));

            while (!MapGenerator.Initialized)
            {
                yield return null;
            }
            
            yield return FindObjectOfType<AgentsGenerator>().GenerateAgents(new OrdersDataImporter(ordersRequestText));
            
            
            Debug.Log("Game Started Successfully");
        }
    }
}