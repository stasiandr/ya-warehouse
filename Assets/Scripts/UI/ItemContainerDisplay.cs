using System.Collections.Generic;
using Storage;
using TMPro;
using UnityEngine;

namespace UI
{
    public class ItemContainerDisplay : MonoBehaviour
    {
        private ItemContainer container;
        
        public Transform rowPrototype;
        
        public ItemContainer Container
        {
            private get { return container; }
            set
            {
                if (container != null)
                    container.ItemCollectionUpdated -= ContainerOnItemCollectionUpdated;
                
                container = value;

                if (container != null)
                {
                    ContainerOnItemCollectionUpdated();
                    container.ItemCollectionUpdated += ContainerOnItemCollectionUpdated;
                }
            }
        }

        [SerializeField, HideInInspector]
        private List<Transform> rows;
        private void ContainerOnItemCollectionUpdated()
        {
            foreach (var row in rows)
            {
                Destroy(row.gameObject);
            }
            rows.Clear();

            foreach (var item in container.Items)
            {
                var row = Instantiate(rowPrototype, rowPrototype.parent);
                row.GetChild(1).GetComponent<TextMeshProUGUI>().text = item.itemID.ToString();
                row.gameObject.SetActive(true);
                rows.Add(row);
            }
        }
    }
}