using System;
using System.Collections.Generic;
using System.Linq;
using Map.Path;
using Misc;
using Miscellaneous;
using UnityEngine;

namespace UI
{
    public class PathRenderer : MonoBehaviour
    {
        public AddressablePool waypointsPool;
        public LineRenderer lineRenderer;
        
        private Path? myPath;

        private List<GameObject> waypoints;
        public Path? MyPath
        {
            get => myPath;
            set
            {
                myPath = value;
                
                RenderPath();
            }
        }

        private void Start()
        {
            waypoints = new List<GameObject>(waypointsPool.prewarm);
        }

        private void RenderPath()
        {
            lineRenderer.enabled = MyPath != null;

            if (MyPath != null)
            {
                lineRenderer.positionCount = MyPath.Value.positions.Count;
                var positions = MyPath.Value.positions.Select(p => p.ToWorldPosition(.5f, .5f)).ToArray();
                lineRenderer.SetPositions(positions);

                foreach (var position in positions)
                {
                    waypoints.Add(waypointsPool.Instantiate(position, Quaternion.identity, transform));
                }
            }
            else
            {
                foreach (var waypoint in waypoints)
                {
                    waypointsPool.Destroy(waypoint);
                }
                waypoints.Clear();
            }
        }
    }
}