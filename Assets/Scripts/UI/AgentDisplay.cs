using System.Linq;
using Agents;
using Misc;
using TMPro;
using UnityEngine;

namespace UI
{
    public class AgentDisplay : MonoBehaviour
    {
        public Agent MyAgent
        {
            private get { return myAgent; }
            set
            {
                myAgent = value;

                if (value == null)
                    pathRenderer.MyPath = null;
                else
                    pathRenderer.MyPath = value.MyPath;
            }
        }

        public TextMeshProUGUI position;
        
        public TextMeshProUGUI nextWayPoint;

        public PathRenderer pathRenderer;
        private Agent myAgent;

        private void Update()
        {
            if (MyAgent == null)
                return;
            
            position.text = MyAgent.Position.ToString();
            nextWayPoint.text = MyAgent.NextWaypoint.ToString();
        }

    }
}