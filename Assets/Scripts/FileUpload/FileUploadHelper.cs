﻿using System;
using System.Collections;
using System.IO;
using UnityEngine;
using Random = UnityEngine.Random;

#if UNITY_EDITOR
using UnityEditor;
#else
using System.Runtime.InteropServices;
#endif


#if UNITY_WEBGL && !UNITY_EDITOR
using UnityEngine.Networking;
#endif

namespace FileUpload
{
    public class FileUploadHelper : MonoBehaviour
    {

        public class FileRequest
        {

            public string URL;
            public string Result;


#if UNITY_WEBGL && !UNITY_EDITOR

            [DllImport("__Internal")]
            private static extern void getFileFromBrowser(string objectName, string callbackFuncName);

#elif UNITY_EDITOR
            // ReSharper disable once InconsistentNaming
            private static void getFileFromBrowser(string objectName, string callbackFuncName)
            {
                _instance.SendMessage(callbackFuncName, EditorUtility.OpenFilePanel("", "", ""));
            }

#else 
            private static void getFileFromBrowser(string objectName, string callbackFuncName)
            {
                throw new NotImplementedException();
            }
#endif

            public IEnumerator FileDialogText()
            {
                if (_instance == null)
                    Initialize();

                _activeRequest = this;

                getFileFromBrowser(_instance.name, "FileCallback");

                while (URL == null)
                    yield return null;
                
#if UNITY_WEBGL && !UNITY_EDITOR

                UnityWebRequest webRequest = UnityWebRequest.Get(URL);
            
                yield return webRequest.SendWebRequest();

                if (webRequest.result == UnityWebRequest.Result.Success)
                {
                    Result = webRequest.downloadHandler.text;
                }
                else
                {
                    throw new Exception();
                }

#elif UNITY_EDITOR

                Result = File.OpenText(URL).ReadToEnd();
                
#else

                throw new NotImplementedException();

#endif
                yield return null;
            }
        }



        private static FileUploadHelper _instance;
        private static FileRequest _activeRequest;

        private static void Initialize()
        {
            _instance = new GameObject($"FileUploadHelper-{Random.Range(0f, 1f)}").AddComponent<FileUploadHelper>();
        }

        public void FileCallback(string url)
        {
                _activeRequest.URL = url;
        }
    }
}
