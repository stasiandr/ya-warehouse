using System;
using System.Collections;
using System.Collections.Generic;
using Game;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Storage
{
    [SelectionBase]
    public class Item : MonoBehaviour
    {
        public event Action TransferItemStarted;
        
        public int itemID;

        private Transform _parent;

        private void OnEnable()
        {
            _parent = transform.parent;
        }

        private void Update()
        {
            if (transform.parent == _parent) return;
            
            TransferItemStarted?.Invoke();
            StartCoroutine(TransferItem());
        }

        private IEnumerator TransferItem()
        {
            yield return new WaitForSeconds(GameSettings.TackingItemTime);
            transform.parent = _parent;
            transform.localPosition = Vector3.zero;
            transform.localRotation = Quaternion.identity;
        }

        public IEnumerator Destroy()
        {
            TransferItemStarted?.Invoke();
            yield return new WaitForSeconds(GameSettings.TackingItemTime / 2);

            Destroy(gameObject);
            yield return new WaitForSeconds(GameSettings.TackingItemTime / 2);
        }

        public void MoveTo(Transform parent)
        {
            _parent = parent;
        }
        
    }
}