using System;
using System.Collections;
using Game;
using Miscellaneous;
using UnityEngine;
using UnityEngine.AddressableAssets;

namespace Storage
{
    public class ItemView : MonoBehaviour
    {
        [SerializeField]
        private Item item;

        [SerializeField] 
        private MeshFilter meshFilter;
        [SerializeField]
        private new MeshRenderer renderer;
        
        [SerializeField]
        private Animator animator;

        [SerializeField]
        public float animationStage;


        private IEnumerator Start()
        {
            while (!ItemsLoader.Initialized) yield return null;

            yield return null;
            
            meshFilter.sharedMesh = ItemsLoader.GetItemMesh(item.itemID);
        }

        private void OnEnable()
        {
            item.TransferItemStarted += ItemOnTransferItemStarted;
        }

        private void OnDisable()
        {
            item.TransferItemStarted += ItemOnTransferItemStarted;
        }

        private void ItemOnTransferItemStarted()
        {
            animator.Play("FadeOutIn");
            StartCoroutine(UpdateAnimationStage());
        }

        private IEnumerator UpdateAnimationStage()
        {
            float length = GameSettings.TackingItemTime;
            while (length > 0)
            {
                renderer.material.color = new Color(1, 1, 1, animationStage);
                length -= Time.deltaTime;
                yield return null;
            }
        }
    }
}