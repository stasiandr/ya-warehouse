using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Misc
{
    public static class Extensions
    {
        public static IEnumerable<Vector2Int> EnumerateIndexes<T>(this T[,] array)
        {
            for (int x = 0; x < array.GetLength(0); x++)
            {
                for (int y = 0; y < array.GetLength(1); y++)
                {
                    yield return new Vector2Int(x, y);
                }
            }
        }

        public static IEnumerable<T> EnumerateRow<T>(this T[,] array, int j)
        {
            for (int i = 0; i < array.GetLength(1); i++)
            {
                yield return array[i, j];
            }
        }

        public static string ListToString<T>(this IEnumerable<T> list, string separator = ", ")
        {
            return string.Join(separator, list);
        }

        public static T GetRandom<T>(this List<T> list)
        {
            return list[Random.Range(0, list.Count)];
        }

        public static void Fill<T>(this T[,] array, T elem)
        {
            foreach (var index in array.EnumerateIndexes())
            {
                array[index.x, index.y] = elem;
            }
        }

        public static T Get<T>(this T[,] array, Vector2Int index)
        {
            return array[index.x, index.y];
        }

        public static void Set<T>(this T[,] array, Vector2Int index, T elem)
        {
            array[index.x, index.y] = elem;
        }

        public static Vector2Int Dimensions<T>(this T[,] array)
        {
            return new Vector2Int(array.GetLength(0), array.GetLength(1));
        }

        public static Vector2Int ToVector2Int(this JToken token)
        {
            return new Vector2Int(token["x"].Value<int>(), token["y"].Value<int>());
        }
        
        public static string MatrixToString<T>(this T[,] array, string verticalSeparator = " ", string horizontalSeparator = "\n")
        {
            return string.Join(horizontalSeparator, Enumerable.Range(0, array.GetLength(0)).Select(i => string.Join(verticalSeparator, array.EnumerateRow(i))));
        }
        

        public static Vector3 ToWorldPosition(this Vector2Int pos, float offset = 0, float y = 0)
            => new Vector3(pos.x + offset, y, pos.y + offset);

        public static Vector2Int ToPositionOnMap(this Vector3 pos)
            => new Vector2Int((int) pos.x, (int) pos.z);

        public static Vector2Int FlattenAndNormalize(this Vector2Int vec)
            => Math.Abs(vec.x) > Math.Abs(vec.y) ? new Vector2Int(Math.Sign(vec.x), 0) : new Vector2Int(0, Math.Sign(vec.y));

        public static bool IsSimpleVector(this Vector2Int vec)
            => vec.x == 0 || vec.y == 0; 
        
        public static bool Adjacent(this Vector2Int a, Vector2Int b)
        {
            Vector2Int diff = a - b;
            return Math.Abs(diff.x) <= 1 && Math.Abs(diff.x) <= 1;
        }

        public static IEnumerable<Vector2Int> Near(this Vector2Int vec)
        {
            yield return vec + Vector2Int.up;
            yield return vec + Vector2Int.right;
            yield return vec + Vector2Int.down;
            yield return vec + Vector2Int.left;
        }

        public static IEnumerable<Vector2Int> Near(this Vector2Int vec, Vector2Int dimensions)
        {
            if (vec.y < dimensions.y - 1)
                yield return vec + Vector2Int.up;
            if (vec.x < dimensions.x - 1)
                yield return vec + Vector2Int.right;
            if (vec.y > 0)
                yield return vec + Vector2Int.down;
            if (vec.x > 0)
                yield return vec + Vector2Int.left;
        }
    }
}