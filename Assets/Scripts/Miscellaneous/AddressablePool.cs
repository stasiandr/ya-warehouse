using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace Miscellaneous
{
    public class AddressablePool : MonoBehaviour
    {
        public AddressableLoader loader;
        public AssetReference asset;

        public int prewarm;

        public bool Initialized { get; private set; }
        
        private int capacity;
        
        private List<GameObject> pool;

        private IEnumerator Start()
        {
            capacity = prewarm;
            Initialized = false;
            
            pool = new List<GameObject>(capacity);
            while (!loader.Initialized)
            {
                yield return null;
            }

            if (prewarm != 0)
                yield return Prewarm();

        }

        private IEnumerator Prewarm()
        {
            pool.AddRange(Enumerable.Repeat<GameObject>(null, capacity));
            
            for (int i = 0; i < prewarm; i++)
            {
                int copy = i;
                asset.InstantiateAsync(transform).Completed += handle =>
                {
                    pool[copy] = handle.Result;
                    pool[copy].SetActive(false);
                };
            }

            while (pool.Any(go => go == null))
            {
                yield return null;
            }

            Initialized = true;
        }

        public GameObject Instantiate(Vector3 position, Quaternion rotation, Transform parent)
        {
            int index = pool.FindIndex(go => !go.activeSelf);

            //TODO implement incrementation
            if (index == -1)
                throw new NotImplementedException();
            
            var t = pool[index].transform;
            t.parent = parent;
            t.position = position;
            t.rotation = rotation;

            pool[index].SetActive(true);
            
            return pool[index++];
        }

        public void Destroy(GameObject go)
        {
            go.SetActive(false);
        }
    }
}