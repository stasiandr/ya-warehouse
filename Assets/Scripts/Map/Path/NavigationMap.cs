using System;
using System.Collections.Generic;
using System.Linq;
using Misc;
using UnityEngine;

namespace Map.Path
{
    public class NavigationMap
    {
        public int[,] HeightMap;

        public static NavigationMap Bake(bool[,] map, bool[,] obstacles)
        {
            var heightMap = new int[map.GetLength(0), map.GetLength(1)];
            
            foreach (var index in map.EnumerateIndexes())
            {
                heightMap.Set(index, map.Get(index) ? 0 : -1);
            }
            
            void ProcessPos(Vector2Int pos)
            {
                int it = heightMap.Get(pos);
                foreach (var near in pos.Near(heightMap.Dimensions()).Where(i => !obstacles.Get(i)))
                {
                    if (heightMap.Get(near) >= it + 1 || heightMap.Get(near) == -1)
                    {
                        heightMap.Set(near, it + 1);
                    }
                }
            }
            
            for (int i = 0; i < map.GetLength(0) * map.GetLength(1); i++)
            {
                foreach (var index in map.EnumerateIndexes().Where(i => !obstacles.Get(i)))
                {
                    ProcessPos(index);
                }
            }
            
            return new NavigationMap {HeightMap = heightMap};
        }

        public Path FindPath(Vector2Int pos)
        {
            var answer = new Path{ positions = new List<Vector2Int>()};

            var it = pos;
            answer.positions.Add(it);
            while (HeightMap.Get(it) != 0)
            {

                Vector2Int newPos = -Vector2Int.one;
                foreach (var position in it
                    .Near(HeightMap.Dimensions())
                    .Where(i => HeightMap.Get(i) != -1))
                {
                    if (HeightMap.Get(position) >= HeightMap.Get(it)) continue;
                    
                    newPos = position;
                    break;
                }

                if (newPos == -Vector2Int.one)
                    throw new ArgumentException();

                it = newPos;
                answer.positions.Add(it);
            }

            return answer;
        }
    }
}