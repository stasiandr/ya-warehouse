using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Gameplay;
using Misc;
using UnityEngine;

namespace Map.Path
{
    [Serializable]
    public struct Path
    {
        public static IEnumerable<Vector2Int> FindPathExclusive(Vector2Int from, Vector2Int to)
        {
            Vector2Int direction = to - from;

            if (direction.x != 0 && direction.y != 0)
                throw new NotImplementedException("A*");

            direction = direction.FlattenAndNormalize();

            Vector2Int current = from + direction;
            while (current != to)
            {
                yield return current;
                current += direction;
            }
        }

        public static IEnumerable<Vector2Int> FindPathInclusive(Vector2Int from, Vector2Int to)
        {
            yield return from;
            foreach (var pos in FindPathExclusive(from, to)) yield return pos;
            yield return to;
        }

        public Path CalculateAgentRoute(HashSet<Vector2Int> waypoints)
        {
            var answer = new Path { positions = new List<Vector2Int> {positions[0]}};

            var lastWaypoint = -Vector2Int.zero;

            for (int i = 1; i < positions.Count; i++)
            {
                foreach (var pos in FindPathInclusive(positions[i - 1], positions[i]))
                {
                    var intersection = waypoints.Intersect(pos.Near()).ToArray();
                    if (intersection.Length != 0)
                    {
                        answer.positions.Add(pos);
                        
                        waypoints.RemoveWhere(p => intersection.Contains(p));

                        if (waypoints.Count != 0) continue;
                        
                        lastWaypoint = pos;
                        break;
                    }
                }
                
                if (waypoints.Count == 0)
                    break;
                    
                answer.positions.Add(positions[i]);
            }

            var toDropZone = MapGenerator.MapToDropZone.FindPath(lastWaypoint);
            toDropZone.Simplify();
            answer += toDropZone;
            
            var toStart = MapGenerator.MapToStart.FindPath(toDropZone.Last);
            toStart.Simplify();
            answer += toStart;

            return answer;
        }
        
        public static Path operator +(Path a, Path b)
        {
            var answer = new Path
            {
                positions =  new List<Vector2Int>(),
                progress = a.IsComplete ? a.progress + b.progress : a.progress
                
            };

            foreach (var pos in a.positions) answer.positions.Add(pos);

            if (a.positions[a.positions.Count - 1] != b.positions[0])
            {
                answer.positions.Add(b.positions[0]);
            }

            for (int i = 1; i < b.positions.Count; i++)
            {
                answer.positions.Add(b.positions[i]);
            }

            return answer;
        }
        public void Simplify()
        {
            if (positions.Count < 2)
                return;
            
            for (int i = 2; i < positions.Count; i++)
            {
                var startIt = positions[i - 2];
                //var midIt = positions[i - 1];
                var nextIt = positions[i];
                while ((nextIt - startIt).IsSimpleVector()) // (midIt - startIt).FlattenAndNormalize() == (nextIt - startIt).FlattenAndNormalize())
                {
                    positions.RemoveAt(i - 1);
                    if (i == positions.Count)
                        break;
                    
                    startIt = positions[i - 2];
                   // midIt = positions[i - 1];
                    nextIt = positions[i];
                }
            }
        }
        
        public List<Vector2Int> positions;

        public int progress;

        public bool IsComplete => positions.Count == progress;
        public Vector2Int Next() => positions[progress++];

        public Vector2Int Last => positions[positions.Count - 1];

    }
}