using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json.Linq;
using UnityEngine;

namespace StaticData
{
    public readonly struct ItemsImporterData
    {
        public readonly struct ItemData
        {
            public readonly int ID;
            public readonly int Amount;
            public readonly Vector2Int Position;

            public ItemData(int id, int amount, Vector2Int position)
            {
                ID = id;
                Amount = amount;
                Position = position;
            }

            public static ItemData FromJson(JToken token)
            {
                return new ItemData(token["id"].Value<int>(),
                    token["amount"].Value<int>(), 
                    new Vector2Int(token["position"]["x"].Value<int>(),
                        token["position"]["y"].Value<int>())
                );
            }
        }

        public readonly ItemData[] Items;

        public int MaxID => Items.Select(i => i.ID).Max();

        public ItemsImporterData(string text)
        {
            var json = JObject.Parse(text);

            Items = json["items"].Select(ItemData.FromJson).ToArray();
        }

        public IEnumerable<ItemData> this[Vector2Int pos]
        {
            get
            {
                return Items.Where(i => i.Position == pos);
            }
        }
    }
}