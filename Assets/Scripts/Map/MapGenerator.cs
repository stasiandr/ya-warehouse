using System;
using System.Collections;
using System.Collections.Generic;
using Gameplay;
using Map.Path;
using Miscellaneous;
using StaticData;
using Storage;
using UnityEngine;
using UnityEngine.AddressableAssets;

namespace Map
{
    public class MapGenerator : MonoBehaviour
    {
        public static MapDataImporter MapData { get; private set; }
        public static ItemsImporterData ItemsData { get; private set; }
        public static Dictionary<Vector2Int, ItemContainer> Shelves { get; private set; }

        public static NavigationMap MapToDropZone { get; private set; }
        public static NavigationMap MapToStart { get; private set; }
        
        public static bool Initialized { get; private set; }
        
        public AddressableLoader resourcesLoader;
        
        public Transform floorPivot;
        public Renderer floorRenderer;

        //public AssetReference shelf;

        public AddressablePool shelvesPool;
        public Transform shelvesContainer;

        public AssetReference agentStart;
        public Transform agentStartContainer;

        public AssetReference agentEnd;
        public Transform agentEndContainer;

        public AssetReference item;
        
        public bool initializeOnStart = true;
        public Texture2D mapTexture;
        public TextAsset itemsLocations;

        // private IEnumerator Start()
        // {
        //     if (!initializeOnStart) yield break;
        //
        //     while (!resourcesLoader.Initialized || !shelvesPool.Initialized)
        //         yield return null;
        //
        //     Initialized = false;
        //     yield return GenerateMap(new MapDataImporter(mapTexture), new ItemsImporterData(itemsLocations.text));
        // }

        public IEnumerator GenerateMap(MapDataImporter mapImporter, ItemsImporterData itemsImporter)
        {
            Initialized = false;
            while (!resourcesLoader.Initialized || !shelvesPool.Initialized)
                yield return null;
                
            
            
            MapData = mapImporter;
            ItemsData = itemsImporter;
            Shelves = new Dictionary<Vector2Int, ItemContainer>();
            MapToDropZone = NavigationMap.Bake(
                MapData.Filter(EntitiesOnMap.DropZone), 
                MapData.Filter(EntitiesOnMap.Shelf));
            
            MapToStart = NavigationMap.Bake(
                MapData.Filter(EntitiesOnMap.Start), 
                MapData.Filter(EntitiesOnMap.Shelf));
            
            floorPivot.localScale = new Vector3(mapImporter.Dimensions.x, 1, mapImporter.Dimensions.y);
            floorRenderer.sharedMaterial.mainTextureScale = (Vector2) mapImporter.Dimensions / 2;

            foreach (var pos in mapImporter.ShelvesPositions)
            {
                // var shelfHandle = 
                //     shelf.InstantiateAsync(new Vector3(pos.x, 0, pos.y), Quaternion.identity,
                //     shelvesContainer);
                // yield return shelfHandle;
                // var container = shelfHandle.Result.GetComponent<ItemContainer>();
                var container = shelvesPool
                    .Instantiate(new Vector3(pos.x, 0, pos.y), Quaternion.identity, shelvesContainer)
                    .GetComponent<ItemContainer>();

                //yield return null;
                
                Shelves[pos] = container;

                foreach (var itemData in itemsImporter[pos])
                {
                    for (int i = 0; i < itemData.Amount; i++)
                    {
                        var itemHandle = item.InstantiateAsync();
                        yield return itemHandle;

                        var itemComponent = itemHandle.Result.GetComponent<Item>();
                        itemComponent.itemID = itemData.ID;
                        container.TryPlaceItem(itemComponent);
                    }
                    
                }

                Initialized = true;
            }

            foreach (var pos in mapImporter.EndPositions)
            {
                yield return agentEnd.InstantiateAsync(new Vector3(pos.x, 0, pos.y), Quaternion.identity,
                    agentEndContainer);
            }
            
            yield return agentStart.InstantiateAsync(new Vector3(mapImporter.StartPosition.x, 0, mapImporter.StartPosition.y), Quaternion.identity,
                agentStartContainer);
        }
    }
}